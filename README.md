# Nerdio

The Backend uses:

- Express.js as http server
- PostgreSQL as database
- Redis as session storage

The Frontend is a React.js SPA.

## Start Backend

```sh
cd backend

# install dependencies
yarn

# start database and redis
docker-compose up -d

# run database schema migrations
npx synor info
npm run synor:migrate:latest

# start
yarn start
```

### OpenAPI Specification

**URL**: `http://localhost:9000/openapi`

## Start Frontend

```sh
cd frontend

# install dependencies
yarn

# start
yarn start
```
