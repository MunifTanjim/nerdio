#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

generate_new_schema_migration_files() {
  local -r migration_dir=$(readlink -f "${DIR}/../migrations")

  if [[ ! -d "${migration_dir}" ]]; then
    mkdir -p "${migration_dir}"
  fi

  local -r version=$(($(date +'%s * 1000 + %-N / 1000000')))

  local -r types=("do" "undo")

  echo "What is the purpose of this schema migration?"
  local title
  read -p ": " title

  title="$(echo "${title}" | sed -e 's/[^a-zA-Z0-9\-_]/-/g')"
  title="$(echo "${title}" | sed -e 's/^-*//g')"
  title="$(echo "${title}" | sed -e 's/-*$//g')"

  if [[ -z ${title} ]]; then
    echo "ERROR: Title is empty!"
    exit 1
  fi

  for _type in "${types[@]}"; do
    local filepath="${migration_dir}/${version}.${_type}.${title}.sql"
    touch ${filepath}
    if [[ "${_type}" = "do" ]]; then
      echo "  DO: ${filepath}"
    elif [[ "${_type}" = "undo" ]]; then
      echo "UNDO: ${filepath}"
    fi
  done
}

generate_new_schema_migration_files
