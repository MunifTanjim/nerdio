begin;

create table "challenge" (
  "id" serial not null
    constraint "challenge_pk" primary key,
  "user_id" integer not null
    constraint "challenge_fk_user_id" references "user" ("id"),
  "text" text not null,
  "difficulty" integer not null default 1,
  "submission_deadline" timestamp with time zone not null,
  "created_at" timestamp with time zone default current_timestamp not null,
  "updated_at" timestamp with time zone default current_timestamp not null
);

create trigger "challenge_on_update_timestamp"
  before update
  on "challenge"
  for each row
  execute procedure on_update_timestamp_trigger();

create table "challenge_user" (
  "challenge_id" integer not null
    constraint "challenge_user_fk_challenge_id" references "challenge" ("id"),
  "user_id" integer not null
    constraint "challenge_user_fk_user_id" references "user" ("id"),
  "created_at" timestamp with time zone default current_timestamp not null,
  "updated_at" timestamp with time zone default current_timestamp not null,
  constraint "challenge_user_pk" primary key ("challenge_id", "user_id")
);

create trigger "challenge_user_on_update_timestamp"
  before update
  on "challenge_user"
  for each row
  execute procedure on_update_timestamp_trigger();
  
create table "challenge_submission" (
  "challenge_id" integer not null
    constraint "challenge_submission_fk_challenge_id" references "challenge" ("id"),
  "user_id" integer not null
    constraint "challenge_user_fk_user_id" references "user" ("id"),
  "text" text not null,
  "created_at" timestamp with time zone default current_timestamp not null,
  "updated_at" timestamp with time zone default current_timestamp not null,
  constraint "challenge_submission_pk" primary key ("challenge_id", "user_id")
);

create trigger "challenge_submission_on_update_timestamp"
  before update
  on "challenge_submission"
  for each row
  execute procedure on_update_timestamp_trigger();

commit;
