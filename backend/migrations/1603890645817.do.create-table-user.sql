begin;

create table "user" (
  "id" serial not null constraint "user_pk" primary key,

  "name" text,
  "bio" text,
  "dob" timestamp with time zone,
  "profession" text,

  "username" text not null constraint "user_uk_username" unique, 
  "email" text not null constraint "user_uk_email" unique,
  "email_verified" bool default false,
  "password" bytea,

  "created_at" timestamp with time zone default current_timestamp not null,
  "updated_at" timestamp with time zone default current_timestamp not null
);

create trigger "user_on_update_timestamp"
  before update
  on "user"
  for each row
execute procedure on_update_timestamp_trigger();

commit;
