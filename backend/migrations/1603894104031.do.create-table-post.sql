begin;

create table "post" (
  "id" serial not null
    constraint "post_pk" primary key,
  "user_id" integer not null
    constraint "post_fk_user_id" references "user" ("id"),
  "text" text not null,
  "parent_post_id" integer default null
    constraint "post_fk_parent_post_id" references "post" ("id"),
  "created_at" timestamp with time zone default current_timestamp not null,
  "updated_at" timestamp with time zone default current_timestamp not null
);

create trigger "post_on_update_timestamp"
  before update
  on "post"
  for each row
  execute procedure on_update_timestamp_trigger();

commit;
