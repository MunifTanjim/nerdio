begin;

drop table if exists "challenge_submission";

drop table if exists "challenge_user";

drop table if exists "challenge";

commit;
