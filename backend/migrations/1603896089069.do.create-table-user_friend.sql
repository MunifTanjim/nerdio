begin;

create table "user_friend" (
  "user_id" integer not null
    constraint "user_friend_fk_user_id" references "user" ("id"),
  "friend_user_id" integer not null
    constraint "user_friend_fk_friend_user_id" references "user" ("id"),
  "created_at" timestamp with time zone default current_timestamp not null,
  "updated_at" timestamp with time zone default current_timestamp not null,
  constraint "user_friend_pk" primary key ("user_id", "friend_user_id")
);

create trigger "user_friend_on_update_timestamp"
  before update
  on "user_friend"
  for each row
  execute procedure on_update_timestamp_trigger();

commit;
