declare namespace Express {
  type SessionData = import('../src/services/session').SessionData

  export interface Request {
    session?: SessionData
  }
}
