require('./src/config/env')

import { PostgreSQLDatabaseEngine } from '@synor/database-postgresql'
import { FileSourceEngine } from '@synor/source-file'
import { ConnectionString } from 'connection-string'
import path from 'path'
import config from './src/config'

type SynorCLIConfig = import('@synor/cli').SynorCLIConfig

const { host, port, username, password, name } = config.get('database')

const databaseUri = new ConnectionString('postgresql://', {
  hosts: [{ name: host, port }],
  user: username,
  password,
  path: [name],
  params: {
    synor_migration_record_table: 'synor_migration_record',
  },
}).toString()

const sourceUri = new ConnectionString(`file://`, {
  path: path.resolve('migrations').split('/'),
  params: {
    ignore_invalid_filename: true,
  },
}).toString()

const synorCLIConfig: SynorCLIConfig = {
  databaseEngine: PostgreSQLDatabaseEngine,
  databaseUri,
  sourceEngine: FileSourceEngine,
  sourceUri,

  baseVersion: '0000000000000',
  recordStartId: 1,
  migrationInfoNotation: {
    do: 'do',
    undo: 'undo',
    separator: '.',
    extension: 'sql',
  },
}

export default synorCLIConfig
