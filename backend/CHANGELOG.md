## 0.1.0 (2020-10-28)


### Features

* **backend:** add avatar colum in user table ([40e3a47](https://gitlab.com/MunifTanjim/nerdio/commit/40e3a47f40a390a88a50d65e0f990bd2d6fccc7f))
* **backend:** add database table user ([976bc43](https://gitlab.com/MunifTanjim/nerdio/commit/976bc43045a8ec8f4be83e390b53a7d06ded34ce))
* **backend:** add libs/redis ([c482f8d](https://gitlab.com/MunifTanjim/nerdio/commit/c482f8d3e9c489ea962c5bbc4bc65450fa4ccbbb))
* **backend:** add middlewares/multer ([3f039aa](https://gitlab.com/MunifTanjim/nerdio/commit/3f039aa3e4ceb6f280ff0b0d42b25549bba301f2))
* **backend:** add models for challenge feature ([a1765f3](https://gitlab.com/MunifTanjim/nerdio/commit/a1765f3bcce47edc1dff0fdfe0c0e0289c42925c))
* **backend:** add posts feature ([932032f](https://gitlab.com/MunifTanjim/nerdio/commit/932032fc29156c878a25ba4f4499fbe897d8cd14))
* **backend:** add signin, signout, signup features ([bd54929](https://gitlab.com/MunifTanjim/nerdio/commit/bd549295ccaf475ad97adb221f68ca5a6ef229c3))
* **backend:** add some /users/* endpoints ([66c1f56](https://gitlab.com/MunifTanjim/nerdio/commit/66c1f5644cb6fa409dc6f1ed1b934cefa1ef3942))
* **backend:** add some endpoints for authenticated user ([084c224](https://gitlab.com/MunifTanjim/nerdio/commit/084c22401e1238968f8753f52d8ab11f9a9f3364))
* **backend:** add user friendship ([2ddc637](https://gitlab.com/MunifTanjim/nerdio/commit/2ddc637fb9ecf6a1acf4aeca84aeb8886474bcbb))
* **frontend:** add initial frontend stuffs ([c1ce81f](https://gitlab.com/MunifTanjim/nerdio/commit/c1ce81f344007af944c5c4f86b597870463c5eb0))


### Bug Fixes

* **backend:** database connection setup ([9dcd7d3](https://gitlab.com/MunifTanjim/nerdio/commit/9dcd7d39cc3f6f4d84d90f7c67be57a43968e469))

