import app from './app'
import http from 'http'

describe('app', () => {
  test('starts', () => {
    const server = http.createServer(app)
    server.listen(app.get('port'))
    server.on('listening', () => {
      // TODO: Call API PATH and check response
    })
  })
})
