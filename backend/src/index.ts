/* eslint-disable @typescript-eslint/no-var-requires */

// https://gist.github.com/branneman/8048520#7-the-hack
process.env.NODE_PATH = __dirname
if (typeof require('module').Module._initPaths === 'function') {
  require('module').Module._initPaths()
}

require('./config/env')

require('express-async-errors')

require('./server')
