import Joi from '@hapi/joi'
import type { Application } from 'express'
import { Router } from 'express'
import ResponseError from 'libs/response-error'
import { validateRequest } from 'middlewares/validate'
import User from 'models/User'

const router = Router()

router.post(
  '/',
  validateRequest({
    body: {
      username: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required().min(8),
      passwordConfirmation: Joi.string().required().valid(Joi.ref('password')),
    },
  }),
  async (req, res) => {
    const { username, email: rawEmail, password } = req.body

    // TODO: Normalize Email
    const email = rawEmail

    const [existingUser] = await User.query()
      .where('username', username)
      .orWhere('email', email)

    if (existingUser) {
      if (existingUser.username === username) {
        throw new ResponseError(400, `Username already claimed!`, [
          {
            message: `Username already claimed!`,
            location: 'username',
            locationType: 'body',
          },
        ])
      }

      if (existingUser.email === email) {
        throw new ResponseError(400, `Email already claimed!`, [
          {
            message: `Email already claimed!`,
            location: 'email',
            locationType: 'body',
          },
        ])
      }
    }

    const user = await User.query().insert({
      username,
      email,
      password,
    })

    // TODO: send verification email
    //
    const data = user.toJSON()

    res.status(201).json({
      data,
    })
  }
)

export function useSignUpController(app: Application): void {
  app.use('/v1/signup', router)
}
