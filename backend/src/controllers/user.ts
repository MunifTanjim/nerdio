import Joi from '@hapi/joi'
import config from 'config'
import type { Application } from 'express'
import { Router } from 'express'
import { move as moveFile } from 'fs-extra'
import ResponseError from 'libs/response-error'
import { authorize } from 'middlewares/authorize'
import { useMulter } from 'middlewares/multer'
import { sessionMiddleware } from 'middlewares/session'
import { validateRequest } from 'middlewares/validate'
import Post from 'models/Post'
import User from 'models/User'
import { join as joinPath } from 'path'

const storageDirectory = config.get('express').storageDirectory

const router = Router()

router.get('/', async (req, res) => {
  const userId = req.session!.user.id

  const user = await User.query().findById(userId)

  const data = user.toJSON()

  res.status(200).json({
    data,
  })
})

router.patch(
  '/',
  validateRequest({
    body: {
      name: Joi.string().optional().allow(''),
      bio: Joi.string().optional().allow(''),
      dob: Joi.date().optional(),
      profession: Joi.string().optional().allow(''),
    },
  }),
  async (req, res) => {
    const userId = req.session!.user.id

    const dataToUpdate = req.body

    const user = await User.query().findById(userId)

    await user.$query().patch(dataToUpdate).returning('*')

    const data = user.toJSON()

    res.status(200).json({
      data,
    })
  }
)

router.post(
  '/avatar',
  useMulter({ allowedMimeType: /image\/.+/ }).single('avatar'),
  async (req, res) => {
    const userId = req.session!.user.id

    const avatarFile = req.file

    if (!avatarFile) {
      throw new ResponseError(400, `Missing file!`, [
        {
          message: 'Missing file!',
          location: 'avatar',
          locationType: 'body',
        },
      ])
    }

    const avatarPath = joinPath(
      storageDirectory,
      `users/${userId}/avatar/${avatarFile.filename}`
    )

    await moveFile(avatarFile.path, avatarPath, { overwrite: true })

    const user = await User.query().findById(userId)

    await user.$query().patch({ avatar: avatarPath })

    const data = user.toJSON()

    res.status(200).json({
      data,
    })
  }
)

router.get('/post-feed', async (req, res) => {
  const userId = req.session!.user.id

  const friends = await User.relatedQuery('friends').for(userId)

  const posts = await User.relatedQuery('posts').for(
    friends.map((friend) => friend.id)
  )

  const items = posts.map((post) => post.toJSON())

  const data = {
    items,
    totalItems: items.length,
  }

  res.status(200).json({
    data,
  })
})

router.get('/posts', async (req, res) => {
  const userId = req.session!.user.id

  const posts = await User.relatedQuery('posts').for(userId)

  const items = posts.map((post) => post.toJSON())

  const data = {
    items,
    totalItems: items.length,
  }

  res.status(200).json({
    data,
  })
})

router.get('/posts/:postId', async (req, res) => {
  const userId = req.session!.user.id
  const postId = Number(req.params.postId)

  const post = await Post.query().findById(postId)

  if (!post) {
    throw new ResponseError(404, `Post(${postId}) not found!`)
  }

  if (post.userId !== userId) {
    throw new ResponseError(403, `not authorized`)
  }

  const data = post.toJSON()

  res.status(200).json({
    data,
  })
})

router.post(
  '/posts',
  validateRequest({
    body: {
      text: Joi.string().required(),
      parentPostId: Joi.number().integer().optional(),
    },
  }),
  async (req, res) => {
    const userId = req.session!.user.id

    const { text, parentPostId } = req.body

    if (parentPostId) {
      const parentPost = await Post.query().findById(parentPostId)

      if (!parentPost) {
        throw new ResponseError(
          404,
          `Parent Post(${parentPostId}) does not exist!`
        )
      }
    }

    const post = await Post.query().insert({
      userId,
      text,
      parentPostId,
    })

    const data = post.toJSON()

    res.status(201).json({
      data,
    })
  }
)

router.get('/friends', async (req, res) => {
  const userId = req.session!.user.id

  const friends = await User.relatedQuery('friends').for(userId)

  const items = friends.map((friend) => friend.toJSON())

  const data = {
    items,
    totalItems: items.length,
  }

  res.status(200).json({
    data,
  })
})

export function useUserController(app: Application): void {
  app.use('/v1/user', sessionMiddleware, authorize())
  app.use('/v1/user', router)
}
