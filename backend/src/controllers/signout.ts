import type { Application } from 'express'
import { Router } from 'express'
import { authorize } from 'middlewares/authorize'
import { sessionMiddleware } from 'middlewares/session'
import { SessionService } from 'services/session'

const router = Router()

router.post('/', authorize(), async (req, res) => {
  await SessionService.destroy(req, res)

  return res.status(204).json({
    data: null,
  })
})

export function useSignOutController(app: Application): void {
  app.use('/v1/signout', sessionMiddleware)
  app.use('/v1/signout', router)
}
