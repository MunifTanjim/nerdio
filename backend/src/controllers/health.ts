import { Router } from 'express'
import type { Application } from 'express'

const router = Router()

router.get('/', (_req, res) => {
  res.status(200).json({
    data: {
      api: 'healthy',
    },
  })
})

export function useHealthController(app: Application): void {
  app.use('/v1/health', router)
}
