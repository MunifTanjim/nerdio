import Joi from '@hapi/joi'
import type { Application } from 'express'
import { Router } from 'express'
import ResponseError from 'libs/response-error'
import { verifyPassword } from 'libs/securepassword'
import { validateRequest } from 'middlewares/validate'
import User from 'models/User'
import { SessionService } from 'services/session'

const router = Router()

router.post(
  '/email',
  validateRequest({
    body: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },
  }),
  async (req, res) => {
    const { email: rawEmail, password } = req.body

    // TODO: Normalize Email
    const email = rawEmail

    const user = await User.query().findOne({
      email,
    })

    if (!user) {
      throw new ResponseError(403, `Invalid credentials!`)
    }

    const isValid = await verifyPassword(user, password)

    if (!isValid) {
      throw new ResponseError(403, `Invalid credentials!`)
    }

    const { token } = await SessionService.create(user, res)

    res.status(200).json({
      data: {
        user,
        token,
      },
    })
  }
)

export function useSignInController(app: Application): void {
  app.use('/v1/signin', router)
}
