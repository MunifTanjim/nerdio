import config from 'config'
import type { Application } from 'express'
import { Router } from 'express'
import ResponseError from 'libs/response-error'
import { authorize } from 'middlewares/authorize'
import { sessionMiddleware } from 'middlewares/session'
import User from 'models/User'
import { transaction } from 'objection'
import { join as joinPath } from 'path'

const storageDirectory = config.get('express').storageDirectory

const router = Router()

router.get('/:userId/avatar', async (req, res) => {
  const userId = Number(req.params.userId)

  const user = await User.query().findById(userId)

  if (!user) {
    throw new ResponseError(404, `User(${userId}) not found!`)
  }

  if (!user.avatar) {
    throw new ResponseError(404, `User(${userId}) has no avatar!`)
  }

  const avatarPath = joinPath(storageDirectory, user.avatar)

  res.status(200).sendFile(avatarPath)
})

router.post('/:userId/action/add-as-friend', async (req, res) => {
  const currentUserId = req.session!.user.id
  const userId = Number(req.params.userId)

  const user = await User.query().findById(userId)

  if (!user) {
    throw new ResponseError(404, `User(${userId}) not found!`)
  }

  const currentUser = await User.query().findById(currentUserId)

  await transaction(User.knex(), async (trx) => {
    await currentUser.$relatedQuery('friends', trx).relate(userId)
    await user.$relatedQuery('friends', trx).relate(currentUserId)
  })

  const data = user.toJSON()

  res.status(200).json({
    data,
  })
})

export function useUserController(app: Application): void {
  app.use('/v1/users', sessionMiddleware, authorize())
  app.use('/v1/users', router)
}
