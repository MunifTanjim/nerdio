import type { Format } from 'convict'
import convict from 'convict'
import ms from 'ms'
import { resolve as resolvePath } from 'path'
import type { LogLevel } from '../libs/logger'
import { validLogLevels } from '../libs/logger'

const timeFormat: Format = {
  name: 'time',
  validate: (value) => {
    ms(value)
  },
  coerce: (value) => {
    if (/^\d+$/.test(value)) {
      return parseInt(value, 10)
    }

    return ms(value)
  },
}

convict.addFormat(timeFormat)

export const config = convict({
  env: {
    doc: 'Environment',
    format: ['development', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },
  port: {
    doc: 'Application Port',
    format: 'port',
    default: 9000,
    env: 'PORT',
  },
  logLevel: {
    doc: 'Log Level',
    format: validLogLevels,
    default: 'info' as LogLevel,
    env: 'LOG_LEVEL',
  },
  cookie: {
    name: {
      doc: 'Cookie Name',
      format: String,
      default: '__nst__',
      env: 'COOKIE_NAME',
    },
    maxAge: {
      doc: 'Cookie Max-Age',
      format: 'time',
      default: ('7d' as unknown) as number,
      env: 'COOKIE_MAX_AGE',
    },
    secret: {
      doc: 'Cookie Secret',
      format: String,
      default: 'c00k13-53cr37',
      env: 'COOKIE_SECRET',
      sensitive: true,
    },
  },
  database: {
    name: {
      doc: 'Database name',
      format: String,
      default: 'nerdio',
      env: 'DATABASE_NAME',
    },
    username: {
      doc: 'Database User',
      format: String,
      default: 'postgres',
      env: 'DATABASE_USERNAME',
      sensitive: true,
    },
    password: {
      doc: 'Database Password',
      format: String,
      default: 'postgres',
      env: 'DATABASE_PASSWORD',
      sensitive: true,
    },
    host: {
      doc: 'Database Host',
      format: String,
      default: '127.0.0.1',
      env: 'DATABASE_HOST',
    },
    port: {
      doc: 'Database Port',
      format: 'port',
      default: 5432,
      env: 'DATABASE_PORT',
    },
  },
  redis: {
    host: {
      doc: 'Redis Host',
      format: String,
      default: '127.0.0.1',
      env: 'REDIS_HOST',
    },
    port: {
      doc: 'Redis Port',
      format: 'port',
      default: 6379,
      env: 'REDIS_PORT',
    },
    password: {
      doc: 'Redis Password',
      format: String,
      default: '',
      env: 'REDIS_PASSWORD',
      sensitive: true,
    },
    db: {
      doc: 'Redis DB',
      format: Number,
      default: 0,
      env: 'REDIS_DB',
    },
  },
  express: {
    storageDirectory: {
      doc: 'Storage Directory',
      format: String,
      default: resolvePath('storage'),
      env: 'EXPRESS_STORAGE_DIRECTORY',
    },
    killTimeout: {
      doc: 'Kill Timeout',
      format: 'time',
      default: ('5s' as unknown) as number,
      env: 'EXPRESS_KILL_TIMEOUT',
    },
  },
})

config.validate({ allowed: 'strict' })

export default config
