import config from 'config'
import { createHash } from 'crypto'
import type { CookieOptions, Request, Response } from 'express'
import log from 'libs/logger'
import { redis, toExpireTimeout } from 'libs/redis'
import { DateTime } from 'luxon'
import type User from 'models/User'
import { getExpiresAt } from 'utils/datetime'
import { v4 as uuidv4 } from 'uuid'

export type SessionData = {
  user: {
    id: number
    username: string
    email: string
  }
}

const cookieConfig = config.get('cookie')

const getCookieOptions = (): CookieOptions => ({
  httpOnly: true,
  secure: config.get('env') === 'production',
  signed: true,
  expires: DateTime.local().plus({ year: 1 }).toJSDate(),
})

const generateSessionKey = (userId: number): string => {
  const uuid = uuidv4()

  const token = createHash('sha512')
    .update(Buffer.from(`${userId}:${uuid}`))
    .digest('hex')

  return token
}

const getCacheKey = (token: string): string => {
  const cacheKey = `session:${token}`
  return cacheKey
}

const extendSessionValidity = (token: string): void => {
  const cacheKey = getCacheKey(token)
  const expiresAt = getExpiresAt('12h')
  redis.expire(cacheKey, toExpireTimeout(expiresAt)).catch((err) => {
    log.error({ err }, `SessionService: failed to extend session validaity`)
  })
}

const create = async (
  user: Pick<User, 'id' | 'username' | 'email'>,
  res: Response
): Promise<{ data: SessionData; token: string }> => {
  const data: SessionData = {
    user: {
      id: user.id,
      username: user.username,
      email: user.email,
    },
  }

  const token = generateSessionKey(user.id)
  const cacheKey = getCacheKey(token)
  const expiresAt = getExpiresAt('12h')
  const stringifiedData = JSON.stringify(data)

  await redis.setex(cacheKey, toExpireTimeout(expiresAt), stringifiedData)

  const cookieOptions = getCookieOptions()

  res.cookie(cookieConfig.name, token, cookieOptions)

  return {
    data,
    token,
  }
}

const destroy = async (req: Request, res: Response): Promise<void> => {
  const token = req.signedCookies[cookieConfig.name]

  if (!token) {
    return
  }

  await redis.del(getCacheKey(token))

  const { expires: _, maxAge: __, ...cookieOptions } = getCookieOptions()

  res.clearCookie(cookieConfig.name, cookieOptions)
}

const getData = async (req: Request): Promise<SessionData | null> => {
  const token = req.signedCookies[cookieConfig.name]

  if (!token) {
    return null
  }

  const stringifiedData = await redis.get(getCacheKey(token))

  if (!stringifiedData) {
    return null
  }

  let data: SessionData | null = null

  try {
    data = JSON.parse(stringifiedData)
  } catch (_) {
    data = null
  }

  if (data) {
    extendSessionValidity(token)
  }

  return data
}

export const SessionService = {
  create,
  destroy,
  getData,
}
