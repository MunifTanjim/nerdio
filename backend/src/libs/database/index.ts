import config from 'config'
import { ConnectionString } from 'connection-string'
import Knex from 'knex'
import log from 'libs/logger'
import { knexSnakeCaseMappers, Model } from 'objection'

const databaseConfig = config.get('database')

const getConnectionUri = (config: {
  host: string
  port: number
  username: string
  password: string
  name: string
}): string => {
  const connectionString = new ConnectionString('postgresql://', {
    hosts: [{ name: config.host, port: config.port }],
    user: config.username,
    password: config.password,
    path: [config.name],
  })

  return connectionString.toString()
}

const masterConnection = Knex({
  connection: getConnectionUri({
    host: databaseConfig.host,
    port: databaseConfig.port,
    username: databaseConfig.username,
    password: databaseConfig.password,
    name: databaseConfig.name,
  }),
  client: 'pg',
  log: {
    debug: log.debug.bind(log),
    error: log.error.bind(log),
    warn: log.warn.bind(log),
  },
  debug: true,
  ...knexSnakeCaseMappers(),
})

export const knex = masterConnection

Model.knex(masterConnection)
