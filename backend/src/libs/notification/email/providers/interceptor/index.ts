import fs from 'fs'
import { DateTime } from 'luxon'
import nodemailer from 'nodemailer'
import os from 'os'
import path from 'path'
import pug from 'pug'
import type { NotiProvider } from 'unoti'
import util from 'util'
import { v4 as uuidv4 } from 'uuid'
import type { EmailParams } from '../../index'

const writeFile = util.promisify(fs.writeFile)

const osPlatform = os.platform()

const getPreviewInstruction = (filePath: string): string => {
  const lines: string[] = []
  switch (osPlatform) {
    case 'linux':
      lines.push(
        `Run the following command to preview email:`,
        `$ xdg-open ${filePath}`
      )
      break
    case 'darwin':
      lines.push(
        `Run the following command to preview email:`,
        `$ open ${filePath}`
      )
      break
    default:
      lines.push(`Open the following file to preview email:`, `  ${filePath}`)
      break
  }

  return lines.join(`\n`)
}

export const getInterceptorEmailProvider = ({
  id = 'interceptor',
  from,
  getId = uuidv4,
  templatePath = path.resolve(
    'src/libs/notification/email/providers',
    'interceptor/preview.pug'
  ),
  storePath = os.tmpdir(),
}: {
  id?: string
  from: NonNullable<EmailParams['from']>
  getId?: () => string
  templatePath?: string
  storePath?: string
}): NotiProvider<EmailParams> => {
  const transporter = nodemailer.createTransport({
    jsonTransport: true,
    from,
  })

  const interceptorProvider: NotiProvider<EmailParams> = {
    id,
    send: async (request) => {
      const info = await transporter.sendMail(request)

      info.message = JSON.parse(info.message)

      const html = pug.renderFile(
        templatePath,
        Object.assign(info.message, {
          cache: true,
          pretty: true,
          date: (info.message.date
            ? DateTime.fromISO(info.message.date)
            : DateTime.local()
          ).toLocaleString(DateTime.DATETIME_MED),
        })
      )

      const filePath = path.join(storePath, `${getId()}.html`)

      await writeFile(filePath, html)

      console.log(`Email Sent!`, getPreviewInstruction(filePath))

      const id = `file://${filePath}`

      return {
        id,
      }
    },
  }

  return interceptorProvider
}
