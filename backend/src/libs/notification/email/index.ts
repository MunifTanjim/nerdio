import htmlToText from 'html-to-text'
import { fallbackStrategy, NotiChannel } from 'unoti'
import type { NotiChannelSenderResponse, NotiProvider } from 'unoti'
import { notiTemplate } from '../template'
import type { TemplateConfig } from '../template'

type EmailAddress = { name: string; address: string } | string

export type EmailParams = {
  id?: string
  date?: Date | string
  from?: EmailAddress
  sender?: EmailAddress
  to?: EmailAddress | Array<EmailAddress>
  cc?: EmailAddress | Array<EmailAddress>
  bcc?: EmailAddress | Array<EmailAddress>
  replyTo?: EmailAddress
  inReplyTo?: EmailAddress
  subject?: string
  text?: string
  html?: string
}

type EmailNotiProvider = NotiProvider<EmailParams>

type Email<Topic extends string> = EmailParams & {
  template?: TemplateConfig<Topic, Array<'html' | 'subject' | 'text'>>
}

export const setupEmailNotification = <Topic extends string = string>({
  channel,
  providers,
}: {
  channel: string
  providers: EmailNotiProvider[]
}): {
  emailChannel: NotiChannel<EmailParams>
  sendEmail: (email: Email<Topic>) => Promise<NotiChannelSenderResponse>
} => {
  const emailChannel = NotiChannel<EmailParams>({
    id: channel,
    providers,
    strategy: fallbackStrategy,
  })

  async function sendEmail(
    email: Email<Topic>
  ): Promise<NotiChannelSenderResponse> {
    const params: Email<Topic> = {
      ...email,
    }

    if (params.template) {
      await Promise.all(
        params.template.params.map((param) =>
          notiTemplate
            .render(
              { channel, topic: params.template!.topic, param },
              params.template!.data,
              params.template!.options
            )
            .then((value) => {
              params[param] = value
            })
        )
      )
    }

    if (!params.text && params.html) {
      params.text = htmlToText.fromString(params.html)
    }

    return emailChannel.send(params)
  }

  return { emailChannel, sendEmail }
}
