import config from 'config'
import { setupEmailNotification } from './email'
import { getInterceptorEmailProvider } from './email/providers/interceptor'
import type { EmailTopic } from './email/topic'

const env = config.get('env')

const emailInterceptor = getInterceptorEmailProvider({
  id: 'interceptor',
  from: {
    name: 'Nerdio',
    address: 'noreply@nerd.io',
  },
})

const emailProvidersByEnvironment: Record<
  string,
  Array<typeof emailInterceptor>
> = {
  development: [emailInterceptor],
}

export const { sendEmail, emailChannel } = setupEmailNotification<EmailTopic>({
  channel: 'email',
  providers:
    emailProvidersByEnvironment[env] || emailProvidersByEnvironment.development,
})
