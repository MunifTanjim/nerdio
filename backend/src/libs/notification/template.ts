/* eslint-disable @typescript-eslint/no-explicit-any */
import mjml2html from 'mjml'
import { resolve as pathResolve } from 'path'
import pug from 'pug'
import { NotiTemplate, renderRaw } from 'unoti'
import type { NotiTemplateRenderer } from 'unoti'

export type TemplateConfig<T extends string, P extends string[]> = {
  topic: T
  params: P
  data?: Record<string, any>
  options?: Record<string, any>
}

const pugRenderer: NotiTemplateRenderer = async (
  templatePath,
  data,
  options = {}
) => {
  const content = pug.renderFile(templatePath, { ...data, ...options.pug })
  return Promise.resolve(content)
}

const mjmlRenderer: NotiTemplateRenderer = async (
  templatePath,
  _data,
  options = {}
) => {
  const mjml = await renderRaw(templatePath)
  return mjml2html(mjml, options.mjml).html
}

const mjmlPugRenderer: NotiTemplateRenderer = async (
  templatePath,
  data,
  options = {}
) => {
  const mjml = await pugRenderer(templatePath, data, options)
  return mjml2html(mjml, options.mjml).html
}

export const notiTemplate = NotiTemplate({
  path: pathResolve('templates'),
  data: {},
  options: {
    mjml: {
      beautify: false,
      validationLevel: 'strict',
    },
    pug: {
      cache: true,
    },
  },
  renderer: {
    txt: renderRaw,
    html: renderRaw,
    pug: pugRenderer,
    mjml: mjmlRenderer,
    'mjml.pug': mjmlPugRenderer,
  },
})
