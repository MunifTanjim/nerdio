import { log } from 'libs/logger'
import User from 'models/User'
import SecurePassword from 'secure-password'

const securePassword = new SecurePassword()

export async function verifyPassword(
  user: User,
  password: string
): Promise<boolean> {
  if (!Buffer.isBuffer(user.password)) {
    return false
  }

  const passwordBuffer = Buffer.from(password)

  const result = await securePassword.verify(passwordBuffer, user.password)

  switch (result) {
    case SecurePassword.INVALID_UNRECOGNIZED_HASH:
      throw new Error('INVALID_UNRECOGNIZED_HASH')
    case SecurePassword.INVALID:
      return false
    case SecurePassword.VALID:
      return true
    case SecurePassword.VALID_NEEDS_REHASH:
      try {
        if (user.id) {
          await User.query().patch({ password }).where('id', user.id)
        }
      } catch (err) {
        log.error(err)
      }

      return true
    default:
      return false
  }
}

export async function hashPassword(password: string): Promise<Buffer> {
  const passwordBuffer = Buffer.from(password)

  return securePassword.hash(passwordBuffer)
}
