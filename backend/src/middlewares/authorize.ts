import { Handler } from 'express'
import ResponseError from 'libs/response-error'

export function authorize(): Handler {
  return async function (req, _res, next): Promise<void> {
    if (!req.session) {
      throw new ResponseError(401, 'Not Authenticated')
    }

    return next()
  }
}
