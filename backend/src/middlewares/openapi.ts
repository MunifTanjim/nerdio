import type { Application } from 'express'
import { ExpressOpenAPI } from 'express-joi-openapi'
import SwaggerUI from 'swagger-ui-express'

export const expressOpenApi = new ExpressOpenAPI({
  useStringStashKey: true,
})

export const useOpenAPISpecification = (app: Application): void => {
  const specification = expressOpenApi.populateSpecification(app)

  const specificationJson = specification.toJSON()

  app.use('/openapi', SwaggerUI.serve, SwaggerUI.setup(specificationJson))
}
