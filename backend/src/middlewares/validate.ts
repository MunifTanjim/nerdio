import type { ErrorRequestHandler } from 'express'
import {
  getJoiRequestValidatorPlugin,
  RequestValidationError,
} from 'express-joi-openapi'
import type { EndpointError } from 'libs/response-error'
import ResponseError from 'libs/response-error'
import { expressOpenApi } from './openapi'

const requestValidationPlugin = getJoiRequestValidatorPlugin()

export const validateRequest = expressOpenApi.registerPlugin(
  requestValidationPlugin
)

export const validationErrorCatcher: ErrorRequestHandler = (
  err: unknown,
  _req,
  _res,
  next
) => {
  if (!(err instanceof RequestValidationError)) {
    return next(err)
  }

  const errors: EndpointError['errors'] = err.validationError.details.map(
    ({ message, path }) => ({
      message,
      location: path.join('.'),
      locationType: err.segment,
    })
  )

  next(new ResponseError(400, 'Validation Error', errors))
}
