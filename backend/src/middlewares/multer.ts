import ResponseError from 'libs/response-error'
import multer from 'multer'
import os from 'os'

const destination = os.tmpdir()

type UseMulterParams = {
  allowedMimeType: RegExp
  limits?: multer.Options['limits']
}

export function useMulter({
  allowedMimeType,
  limits,
}: UseMulterParams): ReturnType<typeof multer> {
  return multer({
    limits: {
      ...limits,
      fieldSize: limits?.fileSize ?? 10 * 1024 * 1024,
    },
    storage: multer.diskStorage({
      destination,
      filename: (_req, file, callback) => {
        const originalName = file.originalname.toLowerCase()
        const fileName = `${Date.now()}-${originalName}`
        callback(null, fileName)
      },
    }),
    fileFilter: (_req, file, callback) => {
      const validMimetype = allowedMimeType.test(file.mimetype)

      if (validMimetype) {
        return callback(null, true)
      }

      callback(new ResponseError(415, 'Unsupported or Malformed file'))
    },
  })
}
