import type { NextFunction, Request, Response } from 'express'
import { SessionService } from 'services/session'

export async function sessionMiddleware(
  req: Request,
  _res: Response,
  next: NextFunction
): Promise<void> {
  const data = await SessionService.getData(req)

  if (data) {
    req.session = data
  }

  next()
}
