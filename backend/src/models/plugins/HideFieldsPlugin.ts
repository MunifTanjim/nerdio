import type { Plugin } from 'objection'

export const HideFieldsPlugin = (columnNames: string[]): Plugin => <M>(
  Model: M
): M => {
  if (!Array.isArray(columnNames)) {
    throw new Error('columnNames must be array')
  }

  // @ts-ignore no other way of doing it...
  return class extends Model {
    $formatJson(json: any) {
      json = super.$formatJson(json)

      columnNames.forEach((columnName) => {
        // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
        delete json[columnName]
      })

      return json
    }
  }
}

export default HideFieldsPlugin
