import type { RelationMappings } from 'objection'
import BaseModel from './BaseModel'
import type Challenge from './Challenge'
import type User from './User'

class ChallengeSubmission extends BaseModel {
  id!: number
  userId!: number
  text!: string

  creator?: User
  challenge?: Challenge

  static relationMappings: RelationMappings = {
    creator: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: 'challengeSubmission.userId',
        to: 'user.id',
      },
    },
    challenge: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'Challenge',
      join: {
        from: 'challengeSubmission.challengeId',
        to: 'challenge.id',
      },
    },
  }
}

export default ChallengeSubmission
