import { hashPassword } from 'libs/securepassword'
import type { ModelOptions, QueryContext, RelationMappings } from 'objection'
import { compose } from 'objection'
import BaseModel from './BaseModel'
import { HideFieldsPlugin } from './plugins/HideFieldsPlugin'
import type Post from './Post'

const Mixin = compose(HideFieldsPlugin(['password']))

class User extends Mixin(BaseModel) {
  id!: number

  name?: string
  bio?: string
  dob?: Date
  profession?: string

  username!: string
  email!: string
  emailVerified!: boolean
  password!: Buffer | string

  avatar?: string

  friends?: User[]
  posts?: Post[]

  static relationMappings: RelationMappings = {
    friends: {
      relation: BaseModel.ManyToManyRelation,
      modelClass: 'User',
      join: {
        from: 'user.id',
        through: {
          from: 'userFriend.userId',
          to: 'userFriend.friendUserId',
        },
        to: 'user.id',
      },
    },
    posts: {
      relation: BaseModel.HasManyRelation,
      modelClass: 'Post',
      join: {
        from: 'user.id',
        to: 'post.userId',
      },
    },
  }

  async $beforeInsert(queryContext: QueryContext): Promise<void> {
    await super.$beforeInsert(queryContext)

    if (typeof this.password === 'string') {
      this.password = await hashPassword(this.password)
    }
  }

  async $beforeUpdate(
    opt: ModelOptions,
    queryContext: QueryContext
  ): Promise<void> {
    await super.$beforeUpdate(opt, queryContext)

    if (typeof this.password === 'string') {
      this.password = await hashPassword(this.password)
    }
  }
}

export default User
