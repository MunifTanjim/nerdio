import { camelCase } from 'lodash'
import { Model } from 'objection'
import path from 'path'

const modelPaths = [path.resolve(__dirname)]

class BaseModel extends Model {
  createdAt!: Date
  updatedAt!: Date

  static modelPaths = modelPaths

  static get tableName(): string {
    return camelCase(this.prototype.constructor.name)
  }
}

export default BaseModel
