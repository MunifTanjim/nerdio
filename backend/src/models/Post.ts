import type { RelationMappings } from 'objection'
import BaseModel from './BaseModel'
import type User from './User'

class Post extends BaseModel {
  id!: number
  userId!: number
  text!: string
  parentPostId!: number | null

  replies?: Post[]
  user?: User

  static relationMappings: RelationMappings = {
    replies: {
      relation: BaseModel.HasManyRelation,
      modelClass: 'Post',
      join: {
        from: 'post.id',
        to: 'post.parentPostId',
      },
    },
    user: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: 'post.userId',
        to: 'user.id',
      },
    },
  }
}

export default Post
