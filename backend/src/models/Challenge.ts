import type { RelationMappings } from 'objection'
import BaseModel from './BaseModel'
import type ChallengeSubmission from './ChallengeSubmission'
import type User from './User'

class Challenge extends BaseModel {
  id!: number
  userId!: number
  text!: string

  creator?: User
  users?: User[]
  submissions?: ChallengeSubmission[]

  static relationMappings: RelationMappings = {
    users: {
      relation: BaseModel.ManyToManyRelation,
      modelClass: 'User',
      join: {
        from: 'challenge.id',
        through: {
          from: 'challengeUser.challengeId',
          to: 'challengeUser.userId',
        },
        to: 'user.id',
      },
    },
    submissions: {
      relation: BaseModel.HasManyRelation,
      modelClass: 'ChallengeSubmission',
      join: {
        from: 'challenge.id',
        to: 'challengeSubmission.challengeId',
      },
    },
    creator: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: 'challenge.userId',
        to: 'user.id',
      },
    },
  }
}

export default Challenge
