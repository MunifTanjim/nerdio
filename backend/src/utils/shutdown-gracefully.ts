import config from 'config'
import type { Server } from 'http'

const expressConfig = config.get('express')

const closeServer = async (server: Server): Promise<void> => {
  return new Promise((resolve, reject) => {
    if (!server.listening) {
      return resolve()
    }

    server.close((err) => {
      if (err) {
        console.error(`Server: failed to close!`)
        return reject(err)
      }

      console.warn('Server: closed!')
      resolve()
    })
  })
}

export const shutdownGracefully = (server: Server): void => {
  console.warn('Received kill signal! Attempting to shutdown gracefully...')

  // we need to stop accepting new requests first.
  // if we close the database connections first instead
  // while still accepting new requests, that's gonna cause trouble.

  closeServer(server)
    .then(() => {
      console.log('Graceful Shutdown: successful!')
      process.exit(0)
    })
    .catch((err) => {
      console.error(err)
    })

  setTimeout(() => {
    console.error('Graceful Shutdown: failed!')
    process.exit(1)
  }, expressConfig.killTimeout)
}
