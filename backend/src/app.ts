import config from 'config'
import { useHealthController } from 'controllers/health'
import { useSignInController } from 'controllers/signin'
import { useSignOutController } from 'controllers/signout'
import { useSignUpController } from 'controllers/signup'
import { useUserController } from 'controllers/user'
import cookieParser from 'cookie-parser'
import express from 'express'
import { useCatcherMiddlewares } from 'middlewares/catchers'
import { useOpenAPISpecification } from 'middlewares/openapi'
import { useRequestLoggerMiddleware } from 'middlewares/request-logger'

const cookieSecret = config.get('cookie').secret

const app = express()

app.use(cookieParser([cookieSecret]))

app.use(express.json())

useRequestLoggerMiddleware(app)

useHealthController(app)

useSignInController(app)
useSignOutController(app)
useSignUpController(app)

useUserController(app)

useOpenAPISpecification(app)

useCatcherMiddlewares(app)

export default app
