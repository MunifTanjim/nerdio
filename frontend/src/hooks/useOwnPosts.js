import { useCallback, useEffect, useState } from 'react';
import api from '../utils/api';

export function useOwnPosts() {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);

  useEffect(() => {
    setLoading(true);
    api('/v1/user/posts')
      .then(({ data }) => {
        setItems(data.items);
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const pushItem = useCallback(item => {
    setItems(items => items.concat(item));
  }, []);

  return { loading, items, pushItem };
}
