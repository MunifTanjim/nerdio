import { Box, Text } from '@chakra-ui/core';
import { get } from 'lodash-es';
import { DateTime } from 'luxon';

function Post({ data }) {
  return (
    <Box p={4} borderWidth="1px" borderColor="white">
      <Text fontSize="sm">
        Posted at:{' '}
        {DateTime.fromISO(get(data, 'createdAt')).toLocaleString(
          DateTime.DATETIME_MED
        )}
      </Text>

      {data.text}
    </Box>
  );
}

export default Post;
