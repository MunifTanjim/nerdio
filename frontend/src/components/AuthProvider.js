import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import api from '../utils/api';

const AuthContext = createContext(null);

export function AuthProvider({ children }) {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    api('/v1/user')
      .then(({ data }) => {
        setUser(data);
      })
      .catch(err => {
        console.error(err);
        setUser(null);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const signIn = useCallback(async ({ email, password }) => {
    setLoading(true);

    const { data } = await api('/v1/signin/email', {
      method: 'POST',
      body: { email, password },
    });

    setUser(data.user);
    setLoading(false);
  }, []);

  const signOut = useCallback(async () => {
    setLoading(true);

    await api('/v1/signout', {
      method: 'POST',
    });

    setUser(null);
    setLoading(false);
  }, []);

  const contextValue = useMemo(() => {
    return { user, setUser, loading, signIn, signOut };
  }, [loading, user, signIn, signOut]);

  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  );
}

export function useAuth() {
  return useContext(AuthContext);
}
