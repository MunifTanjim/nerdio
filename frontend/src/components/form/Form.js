import { VStack } from '@chakra-ui/core';
import React from 'react';
import { FormProvider } from 'react-hook-form';

export function Form({ onSubmit, form, Component = VStack, ...props }) {
  return (
    <FormProvider {...form}>
      <Component as="form" onSubmit={form.handleSubmit(onSubmit)} {...props} />
    </FormProvider>
  );
}
