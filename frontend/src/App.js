import { Center, Container } from '@chakra-ui/core';
import React from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import { PrivateRoute } from './components/PrivateRoute';
import { Dashboard } from './pages/dash';
import SignInPage from './pages/signin';

function App() {
  return (
    <Center minH="100vh">
      <Container maxW="md" centerContent>
        <Router>
          <Switch>
            <Redirect exact from="/" to="/dash" />
            <PrivateRoute path="/dash">
              <Dashboard />
            </PrivateRoute>
            <Route path="/signin">
              <SignInPage />
            </Route>
          </Switch>
        </Router>
      </Container>
    </Center>
  );
}

export default App;
