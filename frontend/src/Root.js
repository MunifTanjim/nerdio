import { ChakraProvider, CSSReset } from '@chakra-ui/core';
import React from 'react';
import App from './App';
import { AuthProvider } from './components/AuthProvider';
import theme from './theme';

function Root() {
  return (
    <ChakraProvider theme={theme}>
      <CSSReset />
      <AuthProvider>
        <App />
      </AuthProvider>
    </ChakraProvider>
  );
}

export default Root;
