import { extendTheme } from '@chakra-ui/core';

const theme = extendTheme({
  styles: {
    global: {
      'html, body': {
        fontSize: 'md',
      },
      body: {
        overflow: 'auto',
      },
      '#root': {
        minHeight: '100vh',
      },
    },
  },
});

export default theme;
