import { Button, HStack } from '@chakra-ui/core';
import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import ProfilePage from '../profile';

export function Dashboard() {
  return (
    <>
      <HStack spacing={4}>
        <Button as={Link} to="/dash/profile" colorScheme="blue">
          Profile
        </Button>

        <Button colorScheme="red">Log Out</Button>
      </HStack>

      <Switch>
        <Route path="/dash/profile" component={ProfilePage} />
      </Switch>
    </>
  );
}
