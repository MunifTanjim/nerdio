import {
  Box,
  Button,
  ButtonGroup,
  Heading,
  Spinner,
  Text,
  useToast,
  VStack,
} from '@chakra-ui/core';
import { get } from 'lodash-es';
import { DateTime } from 'luxon';
import React, { useCallback, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useAuth } from '../../components/AuthProvider';
import { FormButton } from '../../components/form/Button';
import { FormDatePicker } from '../../components/form/DatePicker';
import { Form } from '../../components/form/Form';
import { handleAPIError } from '../../components/form/handleApiError';
import { FormInput } from '../../components/form/Input';
import { FormTextarea } from '../../components/form/TextArea';
import Post from '../../components/Post';
import { useOwnPosts } from '../../hooks/useOwnPosts';
import api from '../../utils/api';

function CreatePost({ parentPostId, onSuccess }) {
  const toast = useToast();
  const form = useForm();

  const onSubmit = useCallback(
    async ({ text }) => {
      const body = { text };

      if (parentPostId) {
        body.parentPostId = parentPostId;
      }

      try {
        const { data } = await api('/v1/user/posts', {
          method: 'POST',
          body,
        });

        onSuccess(data);
        form.reset();
      } catch (err) {
        handleAPIError(err, { form, toast });
      }
    },
    [parentPostId, onSuccess, toast, form]
  );

  return (
    <>
      <Form form={form} onSubmit={onSubmit} my={2}>
        <FormTextarea name="text" label={parentPostId ? 'Reply' : 'Post'} />

        <FormButton type="submit">Submit</FormButton>
      </Form>
    </>
  );
}

function View({ user }) {
  const ownPosts = useOwnPosts();

  return (
    <>
      <VStack>
        <Box>
          <Text>
            <Text as="span" fontWeight="bold">
              Name
            </Text>
            : {get(user, 'name')}
          </Text>

          <Text>
            <Text as="span" fontWeight="bold">
              Bio
            </Text>
            : {get(user, 'bio')}
          </Text>

          <Text>
            <Text as="span" fontWeight="bold">
              Birthdate
            </Text>
            :{' '}
            {get(user, 'dob')
              ? DateTime.fromISO(get(user, 'dob')).toLocaleString(
                  DateTime.DATE_MED
                )
              : null}
          </Text>
        </Box>
      </VStack>

      <Box>
        <Heading as="h3" mb={2}>
          Posts
        </Heading>

        {ownPosts.loading ? (
          <Spinner size="xl" />
        ) : (
          <VStack spacing={2}>
            {ownPosts.items.map(item => (
              <Post key={item.id} data={item} />
            ))}
          </VStack>
        )}

        <CreatePost onSuccess={ownPosts.pushItem} />
      </Box>
    </>
  );
}

function Edit({ user, onDone, onCancel }) {
  const toast = useToast();
  const defaultValues = useMemo(() => {
    return {
      name: get(user, 'name') || '',
      bio: get(user, 'bio') || '',
      dob: get(user, 'dob') || '',
    };
  }, [user]);

  const form = useForm({
    defaultValues,
  });

  const onSubmit = useCallback(
    async ({ name, bio, dob }) => {
      try {
        const { data } = await api('/v1/user', {
          method: 'PATCH',
          body: {
            name,
            bio,
            dob,
          },
        });

        onDone(data);
      } catch (err) {
        handleAPIError(err, { form, toast });
      }
    },
    [form, toast, onDone]
  );

  return (
    <Form form={form} onSubmit={onSubmit}>
      <FormInput name="name" label="Name" />

      <FormTextarea name="bio" label="Bio" />

      <FormDatePicker name="dob" label="Birthdate" />

      <ButtonGroup>
        <FormButton type="submit" colorScheme="blue">
          Update
        </FormButton>
        <FormButton type="button" onClick={onCancel}>
          Cancel
        </FormButton>
      </ButtonGroup>
    </Form>
  );
}

function ProfilePage() {
  const [editing, setEditing] = useState(false);
  const { user, setUser } = useAuth();

  return (
    <Box minW="512px" padding={4}>
      {editing ? (
        <Edit
          user={user}
          onCancel={() => {
            setEditing(false);
          }}
          onDone={updatedUser => {
            setUser(updatedUser);
            setEditing(false);
          }}
        />
      ) : (
        <VStack>
          <View user={user} />
          <Button onClick={() => setEditing(true)}>Edit</Button>
        </VStack>
      )}
    </Box>
  );
}

export default ProfilePage;
