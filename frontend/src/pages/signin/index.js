import { ButtonGroup, Spinner } from '@chakra-ui/core';
import React, { useCallback, useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { Redirect } from 'react-router-dom';
import { useAuth } from '../../components/AuthProvider';
import { FormButton } from '../../components/form/Button';
import { Form } from '../../components/form/Form';
import { FormInput } from '../../components/form/Input';

const getDefaultValues = () => {
  return { email: '', password: '' };
};

function SignInPage() {
  const { user, loading, signIn } = useAuth();

  const defaultValues = useMemo(() => getDefaultValues(), []);

  const form = useForm({
    defaultValues,
  });

  const onSubmit = useCallback(
    async ({ email, password }) => {
      await signIn({ email, password });
    },
    [signIn]
  );

  if (loading) {
    return <Spinner thickness="4px" size="xl" color="primary" />;
  }

  if (user) {
    return <Redirect to="/dash" />;
  }

  return (
    <Form form={form} onSubmit={onSubmit}>
      <FormInput name="email" type="email" label="Email" />
      <FormInput name="password" type="password" label="Password" />

      <ButtonGroup>
        <FormButton type="submit" colorScheme="blue">
          Sign In
        </FormButton>
      </ButtonGroup>
    </Form>
  );
}

export default SignInPage;
