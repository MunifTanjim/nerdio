import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
} from '@chakra-ui/core';
import React from 'react';

export function RootErrorFallback({ error }) {
  return (
    <Alert status="error">
      <AlertIcon />
      <AlertTitle mr={2}>Error:</AlertTitle>
      <AlertDescription>{error.message}</AlertDescription>
    </Alert>
  );
}
